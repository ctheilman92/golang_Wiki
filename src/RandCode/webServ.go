package main

import (
    "fmt"
    "net/http"
)


/*
go build webserv.go to build,
then ./webserv to run webserver on port 8080
attach path name *bananas* to http://localhost:8080/bananas
*/

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hi there I love %s!", r.URL.Path[1:])
}



func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}
