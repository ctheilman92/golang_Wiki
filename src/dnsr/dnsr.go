package main

import (
    "fmt"
    "log"
    "net"
)


/*                  supposedly 0 latency involved, not that i'm anywhere near that point. but you know. some day it'll be worth something   */


func main() {
    
    
    ip := "aetechservices.com"
    ip2 , _ := dnsLookup(ip)
    fmt.Println("aetechservices resovles to -->", ip2)
    fmt.Println()
    
    
    
    //bad host to test bad lookups
    baddhost := "badH0ost.c0m"
    dnsResp, _ := dnsLookup(baddhost)
    fmt.Println("baddH0ost.c0m just shows a nil pointer is printed: --> ", dnsResp)
    fmt.Println()
    
    
    dnsResp2, err := dnsLookup(baddhost)
    if err != nil {
        log.Printf("lookup failed for [ %s ]", baddhost)
    } else {
        fmt.Printf("well tahts not supposed to happen.... it resolved  ->>", dnsResp2)
    }
    
    
    fmt.Println()
    fmt.Println()
    
    
    ovs := "openvswitch.org"
    dnsResp3, errr := dnsLookup(ovs)
    if errr != nil {
        log.Printf("lookup failed for [ %s ]", ovs)
    } else {
        log.Println("openvswitch.org Resolves to: --> ", dnsResp3)
        //log.Printf("[ %s ], resolved to --> [ %s ]", ovs, dnsResp3)
    }
    
    
}


func dnsLookup(ipStr string) (string, error) {
    ipaddr, err := net.ResolveIPAddr("ip", ipStr)
    return ipaddr.String(), err
}

