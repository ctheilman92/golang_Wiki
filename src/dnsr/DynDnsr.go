package main


import (
    "bufio"
    "os"
    "os/exec"
    "fmt"
    "log"
    "net"
) 
    
    
/************

    accept an dns dynamically, until exiting.

************/



func main() {

running := true
for running {    
    
    
    //read user input
    //get bdns host
    fmt.Println("******\n    DNS Resolution ::\n******\n\n")
    reader := bufio.NewReader(os.Stdin)
    fmt.Println("Enter Target Host := ")
    TargetHost, _ := reader.ReadString('\n')
    
    
    //dnslookup
    dnsResp, err := dnsLookup(TargetHost)
    if err != nil {
        log.Printf("could not grab input...]\n")
    } else { 
        fmt.Println("[*]Resolved IP :->", dnsResp)
        fmt.Println()
        fmt.Println()
        fmt.Println()
    }
    
    
    /*exit loop*/
    rdr2 := bufio.NewReader(os.Stdin)
    fmt.Println("##! Would you like to quit now? (Y/n) !##\n")
    ans, _ := rdr2.ReadString('\n')
    if ans != "y" && ans != "no" {
        log.Printf("inappropriate response...\nquitting bitch.")
        break
    } else if ans == "y" {
        fmt.Println("Quitting...")
    } else {
        c := exec.Command("clear").Output
        fmt.Println("%s", c)
    }
    
    
    //endloop    
    }
}
    



func dnsLookup(ipStr string) (string, error) {
    ipaddr, err := net.ResolveIPAddr("ip", ipStr)
    return ipaddr.String(), err
}