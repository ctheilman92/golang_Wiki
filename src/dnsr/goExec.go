package main

import "os/exec"
import "log"


/************

    connecting with system EXEC.
    

************/


func main() {
        
    //send 2 pings to STDOUT(2)
    output1, err := exec.Command("ping", "-c", "2", "8.8.8.8").Output()
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("Ping Results --> %s\n", output1)
    
    
    
    output2, err := exec.Command("date").Output()
    if err != nil {
        log.Fatal(err)
    }   
    log.Printf("Date is --> %s/n", output2)
}