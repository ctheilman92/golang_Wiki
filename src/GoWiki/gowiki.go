package main

import (
    "fmt"
	"io/ioutil"
    "net/http"
    "html/template"
    "regexp"
    "errors"
)

/*****************
    **PAY ATTENTION TO COMMENTS ALWAYS**
******************/


/*********
    GLOBAL VARIABLES
 *********/

 //VAR FOR TEMPLATE CACHING
 //IF ADDING MORE TEMPLATE HTML's ADD STRING TO HERE
 var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

//REGEX FOR SECURITY VALIDATION
//panic if expression fails
var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")






//wiki page construct
type Page struct {
    Title string
    Body []byte     //io library expects byte-slices rather than strings
}

//save method points to Page and saves body to filename
//use type error by the WriteFile library
//return error value, should anything go wrong
//0600 indicates file creation with read-write permissions
func (p *Page) save() error {
    filename := p.Title + ".txt"
    return ioutil.WriteFile(filename, p.Body, 0600)
}


//page loading
//io.ReadFile will return []byte and error
    ////but error isn't handled yet, hence the blank identifier "_"
    
    
func loadPage(title string) (*Page, error) {
    filename := title + ".txt"
    body, err := ioutil.ReadFile(filename)

    //if an error is caught(no such file for example) return nil and the err
    if err != nil {
        return nil, err
    }

    return &Page{Title: title, Body: body}, nil
}




/***********************
************************
web server aspect
************************
***********************/

//PATH VALIDATION FUNC
func getTitle(w http.ResponseWriter, r *http.Request) (string, error) {
    m := validPath.FindStringSubmatch(r.URL.Path)
    if m == nil {
        http.NotFound(w, r)
        return "", errors.New("Invalid Page Title")
    }

    return m[2], nil    // the title is the 2nd subexpression
}


//HTML TEMPLATE CODING USED BY HANDLERS
//EXECUTE var templates
//avoiding coding redundancies throughout different page handler funcs
func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
    err := templates.ExecuteTemplate(w, tmpl+".html", p)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}



//closure wrapper for all HANDLERS
func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
            m := validPath.FindStringSubmatch(r.URL.Path)
        if m == nil {
            http.NotFound(w, r)
            return
        }
	    fn(w, r, m[2])
	}
}

//HANDLERS VIEW, EDIT, & SAVE
//VIEW
func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
    p, err := loadPage(title)
    //nonexistent page handling
    //redirect to edit page with title typed in so to "create"
    if err != nil {
        http.Redirect(w, r, "/edit/"+title, http.StatusFound)
        return
    }

    //USE TEMPLATE FUNC INSTEAD FOR ALL HANDLERS
    renderTemplate(w, "view", p)
}

//EDIT PAGES
func editHandler(w http.ResponseWriter, r *http.Request, title string) {
    p, err := loadPage(title)
    if err != nil {
        p = &Page{Title: title}
    }

        //uses template/html library to parse html files
        // t, _ := template.ParseFiles("edit.html")
        // t.Execute(w, p)

    //using template func
    renderTemplate(w, "edit", p)
}

//SAVE (HANDLES FORM SUBMISSIONS ON EDIT PAGES)
func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
    body := r.FormValue("body")
    p := &Page{Title: title, Body: []byte(body)}    //convert to []byte-slices

    err := p.save()
    if err != nil {
        //any errors occuring during p.save() will be reported to user.
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    http.Redirect(w, r, "/view/"+title, http.StatusFound)
}



func main() {

    fmt.Println("GO webServer running @ http://localhost:/8080/")

        //see make handler as a wrapper for all handlers
    http.HandleFunc("/view/", makeHandler(viewHandler))
    http.HandleFunc("/edit/", makeHandler(editHandler))
    http.HandleFunc("/save/", makeHandler(saveHandler))

    http.ListenAndServe(":8080", nil)

}
